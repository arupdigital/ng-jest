## How Jest has been setup
Remove Karma related stuff.
```
npm remove karma karma-chrome-launcher karma-coverage-istanbul-reporter karma-jasmine karma-jasmine-html-reporter
```
Delete `karma.conf.js` and `test.ts`

Install `jest-preset-angular` and `jest`. 
```
npm i -D jest @types/jest jest-preset-angular
```

In *tsconfig.spec.json*

 - Replace `jasmine` in `types` array with `jest`
 - Remove `test.ts` from `files` array

In *tsconfig.base.json*
  - Create `types` array and add `jest`

In *angular.json* delete `test` configuration from `architect` 

Add `"test:watch": "jest --watch"` and change `"test": "ng test"` to `"test": "jest"` in package.json` scripts

Add the following to `package.json`:
```
  "jest": {
    "preset": "jest-preset-angular",
    "setupFilesAfterEnv": [
      "<rootDir>/src/setupJest.ts"
    ]
  }
```

Create a file called `setupJest.ts` in `src` and add: ```
```
import 'jest-preset-angular';
```

Add `"esModuleInterop": true,` to `tsconfig.base.json`

Add `"emitDecoratorMetadata": true,` to `tsconfig.spec.json`

